In questo capitolo viene descritto nel dettaglio il metodo chiave di profilazione per entità necessario per il calcolo della joinability tra i datasets.\newline
Nella prima sezione viene presentato brevemente il metodo presente inizialmente in Kayak, mentre in quelle seguenti viene definito un nuovo metodo per aumentare la scalabilità ed un'altro approccio per velocizzare ulteriormente la computazione.
\section{Frequenza degli elementi più frequenti}
Questo é uno dei metadati fondamentali che, come vedremo, permette di fare delle analisi importanti sui datasets.\newline
Il calcolo di questo dato é al contempo l'operazione più onerosa, poiché richiede la scansione di tutto il file e diverse operazioni di \textit{MapReduce}.\newline
L'idea è di estrarre per ogni colonna gli elementi più frequenti, che saranno quindi quelli più significativi per comprendere le informazioni contenute nel dataset.\newline
L'output di questa operazione, come visto nel Metadata Catalog, consiste in una Map\textless String\textless Map\textless String, Long\textgreater{}\textgreater, con la prima mappa che indicizza le mappe sulle rispettive colonne, e quella interna che riporta per ogni entità della relativa colonna il numero di occorrenze.\newline
Questa operazione viene eseguita proiettando e mappando una singola colonna per volta. Ad ogni passata vengono calcolati gli elementi più frequenti per una colonna, per poi inserire i valori nella \textit{entry} appropriata.\newline
Quindi, all'aumentare delle colonne, la computazione può subire enormi ritardi poiché non perfettamente parallelizzata.
\section{Nuova frequenza degli elementi più frequenti}
In questa parte del \textit{framework} è stato riscritto il metodo in modo da tenere conto di tutte le colonne con un'unica lettura del file.\newline
\begin{lstlisting}
JavaPairRDD<Tuple2<String, String>, Long> keyMapped = file.flatMapToPair({row => row.split(comaSplitRegex)})
\end{lstlisting}
Con questo unico \textit{mapping} tramite la funzione ''\textit{flatMapToPair}'', vengono restituite in output una serie di \textit{JavaPairRDD} composti da chiavi di tipo \textit{Tuple\textless String, String \textgreater} che hanno dei \textit{Long} come valore.\newline
Ogni riga del file viene divisa utilizzando la \textit{regex} vista precedentemente, ed ogni porzione viene utilizzata come secondo valore della tupla, mentre nel primo vengono salvati i nomi delle colonne, presi dalla lista delle colonne ("columns"), mentre per il valore viene utilizzato un \textit{Long} impostato a 1.\newline
Nello step successivo viene utilizzata l'operazione di reduce sommando semplicemente i valori dei \textit{JavaPairRDD}:
\begin{lstlisting}
JavaPairRDD<Tuple2<String, String>, Long> keyReduced =
keyMapped.reduceByKey((a, b) -> a + b);
\end{lstlisting}
A questo punto viene eseguito un nuovo step di \textit{mapping} seguito da una "\textit{groupByKey}":

\begin{lstlisting}
JavaPairRDD<String, Iterable<Tuple2<String, Long>>> reMap = keyReduced.mapToPair(s -> {
Tuple2<String, Long> value = new Tuple2<>(s._1._2, s._2);
return new Tuple2<String, Tuple2<String, Long>>(s._1._1, value);
}).groupByKey();
\end{lstlisting}
In questo modo le tuple vengono rimappate utilizzando il primo valore della tupla chiave, ovvero il nome della colonna, come nuova chiave, e il secondo valore, cioè l'effettiva occorrenza nel dataset, viene salvato in una tupla insieme alle sue occorrenze, che erano il valore della struttura precedente.\newline
Con la "\textit{groupByKey()}" abbiamo in output un JavaPairRDD mappato su stringhe, che hanno come valore un Iterable\textless Tuple2\textless String, Long\textgreater{}\textgreater, ovvero ad ogni chiave corrisponde una lista di iterabili di tipo tupla.\newline
Successivamente viene effettuato un'ulteriore step di \textit{mapping} nel quale ogni lista iterabile viene raccolta in una struttura più adeguata, ovvero una List\textless Tuple2\textless String, Long\textgreater{}\textgreater : in questo modo la sequenza di valori può venire ordinata in maniera decrescente utilizzando un comparatore creato ad hoc che compara il secondo valore della tupla, cioè il numero di occorrenze del testo nelle rispettive colonne. \newline
La lista viene poi filtrata per prendere al più i primi 100 elementi per motivi di efficienza e anche legati all'hardware in utilizzo, poiché si può facilmente restare a corto di \textit{RAM} allocata alla \textit{JVM} con conseguente \textit{crash} del sistema.\newline
Si può finalmente raccogliere il risultato delle operazioni in \textit{Spark} invocando la funzione "\textit{collectAsMap()}", che restituirà una mappa con tante chiavi quante sono le colonne e degli Iterable\textless Tuple2\textless String, Long\textgreater{}\textgreater come valori.\newline
Gli elementi dell'\textit{Iterable} vengono quindi scorsi e convertiti in \textit{entry} per una \textit{Mappa} di tipo \textless String, Long\textgreater.\newline
In output avremo quindi una \textit{Mappa} con valori di tipo \textit{Stringa} legati a loro volta alla mappa definita sopra.\newline
Da notare che prima di essere restituita in output, nei valori chiave vengono rimpiazzati eventuali caratteri di tipo "," con "." ed eventuali simboli "\textdollar" ad inizio stringa poichè da specifiche il \textit{database} in utilizzo per mantenere i metadati, ovvero \textit{MongoDB}, non accetta stringhe con "," o inizianti con "\textdollar" per i valori chiave.\newline
\section{Sampling dei file}
Un altra tecnica per velocizzare il calcolo di questa struttura dati cruciale per la profilazione, è quella del \textit{sampling} dei file, o campionamento.\newline
Si può infatti facilmente intuire che sarà nettamente più veloce l'analisi di una porzione del file rispetto alla sua totalità\newline
Tuttavia, un sampling del file per passarlo successivamente alla funzione che calcola gli elementi più frequenti potrebbe portare addirittura ad un ritardo, dovendo scorrere il file una volta per selezionarne i campioni da iterare nuovamente per estrarre poi le frequenze.\newline
Per risolvere questo problema, lo stesso sampling del file viene effettuato tramite la funzione \textit{sample()} offerta dalla classe \textit{JavaRDD}.\newline
Questo metodo prende in input 3 elementi:
\begin{itemize}
	\item un \textit{booleano} che permette o nega il sampling della stessa riga;
	\item un \textit{double} tra 0 e 1 che indica la percentuale di campionamento;
	\item un \textit{seed} di tipo \textit{long} per garantire la randomicità del sampling.
\end{itemize}
Ovviamente, il primo parametro è bene che sia settato a \textit{false}, poiché nello scenario peggiore, anche se inverosimilmente, una riga potrebbe venir campionata ripetutamente facendo aumentare la frequenza dei suoi elementi durante la computazione, mentre magari si tratta di una riga unica con elementi che non si ripetono nel file.\newline
Per quanto riguarda il \textit{seed} invece, per garantire la maggior randomicità possibile, viene generato basandosi sull'orario interno della macchina al momento dell'invocazione del metodo.\newline
Le stime sullo \textit{speed up} garantito dall'approccio di campionamento e sulla perdita di precisione che inevitabilmente può inserire viene rimandata al capitolo dei risultati.