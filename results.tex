Le varie tecniche finora descritte per la profilazione dei datasets sono state analizzate per capire i vantaggi che possono apportare al sistema e farne un confronto anche in termini di ottimizzazione delle performance in relazione al tempo ed alla precisione.\newline
Per questo motivo è stato messo a punto un processo di profilazione di tutti i dataset del datalake con successiva elaborazione di metadati inter-datasets per ognuno di essi. L'idea è di simulare uno scenario nel quale l'utente fornisce una serie di datasets in input e per ognuno di essi vengono calcolate le joinabilities con gli altri presenti nel sistema.\newline
I diversi approcci possono essere confrontati sostanzialmente su due scale: il tempo di computazione e la precisione.\newline
Per quanto riguarda il primo fattore, è facile intuire che sarebbe preferibile avere tempistiche di esecuzioni minori, mirando ad offrire all'utente nel minor tempo possibile datasets di interesse a partire da quello che inserisce nel sistema, per poi in caso raffinare i risultati con computazioni successive più precise.\newline
Diverso è invece il discorso per la precisione: in questo caso è necessario avere un "\textit{ground truth}", ovvero una serie di valori considerabili esatti a cui confrontare quelli calcolati dagli algoritmi di profilazione. Per avere questa base di partenza vengono raccolte tutte le entità di tutte le colonne, in modo da calcolare una joinability completa tra le colonne dei vari datasets, come avviene nel metodo "AllEntitiesAndDucc", a cui rifarsi per validare le metodologie che tendono ad approssimare il risultato per offrirlo in tempo minore.\newline
I risultati sono basati sull'utilizzo di 155 datasets di varia natura e provenienza estratti tramite \cite{dataQuest} e da altre fonti, con dimensione variabile tra i pochi KB fino ad arrivare a alcuni datasets con dimensione superiore ai 500MB.\newline
I test sono stati effettuati su macchina virtuale con sistema operativo Linux e 5GB di RAM allocati, utilizzando un deploy di Spark con nodo singolo, che fa intuire ulteriori possibilità di riduzione dei tempi computazionali con l'utilizzo di più nodi per parallelizzare il calcolo.\newline
La dimensione dei files non è considerabile nel contesto "Big Data", tuttavia l'implementazione delle tecniche utilizzandone i tools relativi ed i test effettuati garantiscono con buona probabilità che, scalando la dimensione dei files e l'hardware in uso, gli andamenti seguano le rilevazioni estratte con questi a disposizione.\newline
 Il resto del capitolo è organizzato come segue: nella prima sezione viene descritta la classe astratta "ProfilingMethod" che implementa i diversi iter di profilazione del data lake, nella successiva vengono confrontati i due approcci di estrazione della chiave valutandone la precisione, in quella seguente vengono messi a confronto i diversi metodi di estrazione degli elementi più frequenti in base a tempi di esecuzione e precisione, mentre nell'ultima sezione vengono analizzati i risultati relativi al calcolo approssimato di joinabilities e affinità.
\section{I metodi di profilazione}
Il metodo di profilazione generale viene definito nella classe astratta "ProfilingMethod", che, come vedremo, viene poi estesa dai vari possibili approcci risultanti dall'integrazione delle tecniche messe a punto. \newline
Essa offre un unico metodo pubblico chiamato "execute" che prende in input i parametri iniziali passati dall'utente per avviare l'"InsertCommand" discusso nel primo capitolo.\newline
In questo metodo vengono inizialmente estratti tutti i metadati di contorno necessari per avviare la profilazione del dataset in input. A questo punto viene invocato il metodo astratto "profileDataset", che viene riscritto per ogni diverso approccio, come andremo a vedere a breve. Questo metodo restituisce un DatasetProfile dopo averlo inserito nel database.\newline
Una volta profilato il dataset di partenza, viene scorsa la lista degli elementi presenti nel data lake e vengono anch'essi profilati ad uno ad uno, popolando il Global Dataset Metadata che poi servirà per estrarre metadati inter-dataset.\newline
Questa classe offre altri metodi di tipo "\textit{protected}" utilizzabili dalle classi che la andranno ad estendere per combinare le diverse elaborazioni, in particolare:
\begin{itemize}
	\item \emph{computeKeysWithoutDUCC} : questo metodo offre un rapido tentativo di estrazione della chiave utilizzando il metodo poco robusto descritto precedentemente;
	\item \emph{computeKeysWithDUCC} : in questo caso viene invece preparato ed eseguito l'algoritmo \textit{DUCC} sul dataset, e vengono salvate le chiavi eventualmente trovate nel DatasetProfile;
	\item \emph{computeOntologies} : questo metodo permette di utilizzare l'estrattore delle ontologie e salvarne i risultati nel profilo del dataset;
	\item \emph{computeTopFrequentsWithOldMethod} : questa funzione permette di calcolare gli elementi più frequenti del dataset utilizzando la tecnica utilizzata inizialmente in Kayak che scorre il file tante volte quante colonne lo compongono;
	\item \emph{computeTopFrequents} : in questo caso invece gli elementi più frequenti vengono estratti scorrendo il file una sola volta;
	\item \emph{computeAllEntities} : questa funzione riporta tutti gli elementi di ogni colonna ed il numero delle loro occorrenze per calcolare la joinability esatta;
	\item \emph{computeTopFrequentsSampled} : questo metodo permette di calcolare gli elementi più frequenti su un campione randomico del file;
	\item \emph{initializeProfile} : questa funzione prepara il DatasetProfile iniziale le cui informazioni verranno utilizzate per eseguire i metodi elencati sopra.
\end{itemize}
Combinando le funzioni appena elencate, è stato possibile definire diversi metodi di profilazione che vengono riportati qui di seguito che si basano sulle linee guida di quello descritto nel capitolo precedente.\newline Sostanzialmente ogni metodologia viene espressa da una classe che estende "ProfilingMethod" e riscrive il metodo astratto "profileDataset" combinando i metodi di cui sopra.\newline
Una suddivisione ulteriore di questi approcci può essere identificata nel confronto tra il vecchio metodo di calcolo dei "Top Frequent" e quello nuovo.\newline
Avremo quindi banalmente una classe "\textit{TopFrequent}" ed una "\textit{OldTopFrequent}", che estraggono solo gli elementi più frequenti ed utilizzano la tecnica di estrazione delle chiavi senza l'utilizzo di DUCC. Questi due metodi arrivano agli stessi risultati, ma hanno, come risulta dalle sezioni seguenti, differenti tempi di esecuzione.\newline
Altre due classi possono essere messe a confronto, ovvero \textit{TopFrequentAndDucc} e \textit{OldTopFrequentAndDucc}: l'unica differenza con i metodi precedenti sta nell'esecuzione dell'algoritmo DUCC che dovrebbe portare ad un'estrazione della chiave più precisa a discapito del tempo di esecuzione. C'è comunque da tenere in considerazione l'eventuale guadagno che questa tecnica potrebbe apportare limitando il numero di colonne da analizzare con gli strumenti successivi.\newline
A questo punto viene preso in considerazione un altro elemento del DatasetProfile, ovvero le ontologie.\newline
Con le due classi \textit{TopFrequentDuccAndOntologies} e \textit{TopFrequentAndOntologiesWithoutDUCC} viene inserita la possibilità di estrarre le ontologie per gli elementi più frequenti da \textit{DBPedia}. In questo modo le tempistiche di elaborazione vanno ad aumentare considerando la necessità di effettuare richieste HTTP, ma si può assumere di averle in locale per effettuare un confronto con gli altri metodi in termini di precisione.\newline
A queste 6 metodologie di profilazione si vanno ad affiancare quelle che effettuano un sampling del file caricato su Spark prima di calcolare le frequenze degli elementi. È possibile impostare la percentuale di sampling e l'estrazione avviene in maniera randomica. Quindi l'idea per questi approcci è quella di stimare quanto si perda in precisione per avere dei guadagni sul tempo di esecuzione.\newline
In particolare possono essere identificate 4 diverse classi che effettuano il sampling prima di profilare i datasets:
\begin{itemize}
	\item \emph{SampledTopFrequent}
	\item \emph{SampledTopFrequentAndDucc}
	\item \emph{SampledTopFrequentDuccAndOntologies}
	\item \emph{SampledTopFrequentAndOntologiesWithoutDucctext}
\end{itemize}
Il comportamento dei vari approcci è facilmente deducibile rifacendosi a quanto detto per quelli precedenti.
 \section{Estrazione delle chiavi}
 Come abbiamo detto in precedenza, individuare le colonne chiave prima di una profilazione potrebbe ridurre significativamente la porzione di file da analizzare, diminuendo a sua volta il tempo di elaborazione e l'attesa dell'utente che effettua una query al sistema.\newline
 Per validare le chiavi identificate tramite i due metodi, è stata necessaria un'etichettatura manuale di ogni singolo file, avendo così una base di confronto.
 \begin{figure}[h]
 	\centering
 	\includegraphics[width=\textwidth]{figure/keysEval}
 	\caption{Confronto dell'estrazione delle chiavi}
 	\label{fig:keysEval}
 \end{figure}
\newline
 Come si può notare, i risultati si attestano in entrambi i casi su buoni valori: un 84\% di precisione per il primo, e addirittura 88\% per l'algoritmo Ducc.\newline
 Questo potrebbe dunque permettere un \textit{pruning} iniziale grazie all'utilizzo di Ducc, tuttavia in caso l'analisi richieda una ottima precisione questa via non potrebbe essere molto percorribile poiché si perderebbe la possibilità di trovare matchings su una parte dei datasets, in questo caso intorno al 12\%.\newline
 Il metodo rudimentale non è invece orientato a questo scopo, essendo stato implementato a priori solo per una pura conoscenza delle chiavi, senza farne utilizzo in alcun modo, e applicarlo risulta impossibile basando il suo funzionamento sugli elementi più frequenti, ovvero successivamente alla profilazione più onerosa del framework.\newline
Inoltre, un'eventuale rimescolamento delle colonne del dataset potrebbe portare a risultati più bassi dato che questa tecnica assume la presenza delle colonne chiave tra le prime del dataset.\newline
Dall'altro lato, Ducc non soffre di questi due problemi e potrebbe intervenire nelle prime fasi di \textit{preview} con soluzioni approssimate.
\section{Tempi di esecuzione}
Essendo l'implementazione basata su tools e tecniche di ambito "Big Data", si può assumere che andando ad aumentare il numero di files in input i dati riportati possano seguire la tendenza che assumono elaborando la quantità di datasets a disposizione.
\subsection{TopFrequent vs OldTopFrequent}
Questi due approcci possono essere considerati alla pari in termini di precisione, ma è interessante invece andare a verificare se effettivamente l'intervento sul codice ha portato ad un miglioramento sulle tempistiche di calcolo degli elementi più frequenti.
 \begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figure/topFvsOldTopF}
	\caption{Confronto tempi di profilazione}
	\label{fig:topFvsOldTopF}
\end{figure}
\newline
I risultati in effetti testimoniano un guadagno in termini di tempo di circa il 20\%: infatti dai circa 160 secondi totali di esecuzione si scende intorno ai 130, seguendo l'andamento definito lanciando il processo più volte aumentando il numero di files. Questo fa intuire che all'aumentare dei dataset si avranno vantaggi ulteriori sulle tempistiche di elaborazione, quindi il nuovo metodo può essere utilizzato rimpiazzando quello precedente.
\subsection{TopFrequentAndDucc vs OldTopFrequentAndDucc}
Il confronto tra questi due metodi è necessario principalmente per validare il confronto tra i precedenti, ma anche per verificare di quanto l'introduzione di DUCC possa rallentare la computazione.\newline
Si può notare che la differenza si mantiene sul 20\% circa, considerando che il primo ha una media di esecuzione di 142 secondi e l'altro di 180.
 \begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figure/duccDelay}
	\caption{Delay aggiunto da DUCC}
	\label{fig:duccDelay}
\end{figure}
\newline
Da tenere in considerazione è però sia il costo computazionale del suo utilizzo, che non si basa su strumenti orientati ai "Big Data" e potrebbe quindi non scalare insieme al metodo di estrazione degli elementi più frequenti, sia la possibilità di perdere alcune informazioni come visto nella prima sezione.
\subsection{Normal vs Sampled}
A questo punto si possono effettuare considerazione più o meno identiche per quanto riguarda l'approccio con \textit{sampling} paragonandolo a quello normale.\newline
In particolare, per i test la percentuale di sampling è stata impostata al 20\%: andando a confrontare \textit{TopFrequent} e \textit{SampledTopFrequent}, si identifica una differenza del 67\% sui tempi di esecuzione, considerando che nel secondo caso la media con input massimo scende a circa 88 secondi.
 \begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figure/methodsTime}
	\caption{Tempi di esecuzione a confronto}
	\label{fig:methodsTime}
\end{figure}
\newline
Anche in questo caso, si può mettere in parallelo il confronto tra \textit{TopFrequentAndDucc} e \textit{SampledTopFrequentAndDucc}, verificando che la differenza resta intorno al 70\%, e all'aumentare dei dataset presenti nel data lake il divario tende a favorire il metodo con sampling.
\subsection{Calcolo delle ontologie}
Il calcolo delle ontologie, come detto precedentemente, aggiunge un \textit{delay} eccessivo e non confrontabile con gli altri, dovendo maggior parte dell'attesa alla qualità della connessione.\newline
Nei risultati si è assunto di avere in locale l'intero KG da interrogare, quindi il tempo di calcolo delle ontologie viene considerato pari a 0 dovendo semplicemente cercare 10 entry, in particolare le più frequenti, per ogni colonna.\newline
Le considerazioni al riguardo di questa tecnica di profilazione vengono posticipate alla prossima sezione, non essendoci molto da discutere relativamente ai tempi di esecuzione.
\section{Precisione degli approcci}
In quest'ultima sezione vengono riportati i risultati più interessanti, quelli relativi alla precisione dei due processi di profilazione, ovvero quello che utilizza gli elementi più frequenti per calcolare la joinability tra i datasets, e quello basato sulle ontologie per misurare l'affinità tra essi.\newline
Come vedremo, in entrambi i casi sono stati utilizzati i risultati ottenuti calcolando le joinabilities precise con il metodo "AllEntitiesAndDucc".
\subsection{Precisione della profilazione per entità}
Per misurare la precisione della profilazione per entità, basandosi sulle joinabilities calcolate con il metodo che non seleziona solo gli elementi più frequenti bensì tutti quelli presenti in ogni colonna, è stata utilizzata una soglia crescente che escludesse man mano più elementi dal \textit{groundtruth}.\newline
Questo perché bisogna tenere a mente che il metodo "AllEntitiesAndDucc" riesce a trovare joinabilities tra colonne che possono essere fuorvianti: basta infatti che un valore appaia una volta in due colonne diverse per far sì che esse vengano collegate da questa relazione. In alcuni casi questo può portare ad identificare come joinabili due colonne per la presenza di un elemento, e ciò accade soprattutto nel caso di valori numerici, che nei due datasets indicano probabilmente informazioni di tipo totalmente diverso.\newline
Nel confronto sono stati inseriti solo i risultati relativi all'esecuzione di "TopFrequent" e di "SampledTopFrequent", considerando che le altre derivazioni riportano gli stessi risultati, poiché la differenza con questi metodi è relativa ad altri indicatori.
 \begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figure/joinabilitiesPrecision}
	\caption{Precisione della profilazione per entità}
	\label{fig:joinabilitiesPrecision}
\end{figure}
\newline
Si può notare come il metodo che utilizza il sampling non riesca a identificare tutte le joinabilities minori, ma riesce a performare comunque bene praticamente quanto l'altro non considerando quelle di valore inferiore a 0.1.\newline
All'aumentare della dimesione dei dataset questa differenza tende a diminuire ulteriormente considerando le joinabilities basse ancora sempre meno influenti.
\subsection{Precisione delle ontologie}
Lo studio sulla precisione del metodo delle ontologie, ovvero a livello di affinità tra due colonne di diversi datasets, ha fatto uso di una soglia ancora più variabile.\newline
In questo caso infatti, sui datasets con bassa joinability, ci aspettiamo di non identificare affinità, intesa come presenza di ontologie in comune, tra due files.\newline
Per alti valori di joinability ci aspettiamo invece che un tipo di relazione a livello di ontologie venga identificata dal processo di profilazione.\newline
Anche in questo caso sono stati riportati i risultati di due procedimenti: "TopFrequentDuccAndOntologies" e la sua versione che utilizza il sampling, "SampledTopFrequentDuccAndOntologies".\newline
 \begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figure/ontologiesPrecision}
	\caption{Precisione della profilazione per ontologie}
	\label{fig:ontologiesPrecision}
\end{figure}
\newline
Si può notare come considerando tutte le joinabilities la precisione delle relazioni di affinità sia molto bassa, ma non appena si fa variare la soglia essa comincia giustamente a crescere, fino ad arrivare sopra l'80\% per le joinabilities di valore più elevato, facendo capire come anche l'approccio sull'affinità sia corretto e possa intervenire per convalidare la joinability di una coppia di colonne.