Kayak si propone di aiutare i data scientists a ridurre il "time-to-action", ovvero il tempo che li separa dal poter iniziare ad effettuare analisi redditizie sui dati ordinati ed estratti dal data lake\cite{kayakPaper}.\newline
Esso esegue dei lavori di preparazione dei dati ed esplorazione del data lake a livello incrementale, fornendo delle rapide anteprime che sono raffinate man mano che il risultato esatto viene calcolato, come avviene in \cite{blinkDB} o in \cite{daq}.\newline
Il framework si basa su una gestione oculata dei metadati che permette un limitato intervento umano e fornisce un contesto ai datasets nel lago.\newline
Ai nostri giorni, il successo di un'azienda è inevitabilmente intrecciato all'abilità di raccogliere ed analizzare velocemente i dati generati ogni giorno in grandi quantità.\newline
Da un lato l'azienda deve essere reattiva ai cambiamenti che avvengono ormai su base giornaliera, dall'altra parte il settore IT non riesce a tener dietro a questo cambiamento continuo.\newline
Nei tradizionali modelli di "Bussiness Intelligence", termine che raccoglie i processi aziendali per raccogliere dati ed analizzare informazioni strategiche, le tecnologia utilizzate per realizzare questi processi e le informazioni ottenute come risultato \cite{WIKI:BUSSINESSINTELLIGENCE}, attività come la modellazione, l'estrazione, la pulizia e la trasformazione dei dati sono necessari, ma sfortunatamente questo rende l'integrazione delle nuove funzionalità con quelle già presenti un processo molto lungo.\newline
Per superare il problema, alcune società cercano di annullare o comunque diminuire l'intervento umano fino all'effettivo sfruttamento dei dati.\newline
Questo approccio al mantenimento dei dati è detto appunto "data lake" e consiste nel conservare i dati grezzi di qualsiasi tipo nel loro formato originale all'interno di sistemi di archiviazione.\newline
I data scientists vengono supportati da motori di ricerca che generano indici dei dati, o da altri \textit{tools} che prevedono lunghi processi di profilazione per identificare correlazioni e dipendenze tra gli attributi, o analisi statistiche: queste elaborazioni rappresentano il collo di bottiglia per i nuovi dataset che arrivano continuamente nel data lake.\newline
In questo scenario Kayak si propone dunque come un framework di gestione di dati su data lake, che permette di scalare bene all'aumentare dei file memorizzati, e soprattutto abbatte il tempo dedicato alla preparazione ed all'esplorazione dei dati.\newline
Le idee chiave di Kayak sono principalmente così riassumibili:
\begin{itemize}
	\item prima l'utente riesce ad avere informazioni sui dati, prima potrà definire concretamente il tipo di analisi da effettuare. Poiché spesso un risultato approssimativo può essere sufficiente all'inizio di una ricerca prima di entrare più in profondità, Kayak offre delle strategie per dividere le funzioni dell'applicazione in compiti che riportano risultati via via più precisi.
	\item le meta-informazioni relative ai files presenti nel data lake vengono utilizzate per garantire un'accesso veloce agli stessi. Anche l'estrazione di questi meta-dati viene effettuata utilizzando strategie di tipo incrementale.
	\item per sfruttare la grande mole di dati, le meta-informazioni non devono essere solo di tipo \textit{intra-dataset}: in Kayak infatti viene creato uno schema che rappresenta l'\textit{"affinità"} e la \textit{"joinability"} tra i dataset del lago, concetti che verranno approfonditi nel capitolo successivo.
	\item i principi di scalabilità e modularità vengono rispettati potendo eseguire le operazioni in un'ambiente distribuito e parallelo, inoltre il riuso dei \textit{task} principali di Kayak permette una semplice implementazione di nuove \textit{features} .
\end{itemize}
Il resto del capitolo è organizzato come segue: nella prima sezione viene presentata l'architettura generale di Kayak, mentre in quelle seguenti vengono descritti più dettagliatamente i moduli coinvolti nello svolgimento di questo lavoro: in particolare il \textit{Metadata Collector}, l'\textit{Ontology Extractor} ed il \textit{Metadata Catalog}.
\section{L'architettura ed il funzionamento}
In questa sezione viene mostrata l'architettura di Kayak presentandone le varie componenti.
\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figure/kayakArchitecture}
	\caption{L'architettura generale di Kayak}
	\label{fig:kayakArc}
\end{figure}
\newline
Come si può vedere dalla figura \ref{fig:kayakArc}, Kayak offre un'interfaccia visuale, o \textbf{User Interface}, fruibile a tutti gli utenti. Tuttavia, le primitive di Kayak sono fruibili anche tramite \textbf{APIs} per permettere l'interazione anche ad applicazioni di terze parti.\newline
Le primitive invocate tramite le interfacce vengono ricevute dal \textbf{Task Generator}, che rappresenta il nodo di accesso a tutto il framework. Esso decide se utilizzare strategie per l'esecuzione incrementale delle tasks. Al suo interno include un \textit{Dependency Manager} che stabilisce priorità e dipendenze all'interno delle primitive, ed un \textit{Cost Estimator} che determina il costo delle singole tasks. Infine, uno \textit{Scheduler} si occupa di assegnare l'ordine di esecuzione.\newline
La generazione e l'esecuzione delle tasks viene disaccoppiato dal \textit{Queue Manager}. Esso gestisce un sistema di code con l'aiuto di diversi processi produttori e consumatori di messaggi. Questo permette un'elaborazione asincrona, avviando un task non appena le dipendenze dagli altri tasks vengono risolte.
I tasks nella coda passano al \textbf{Task Executor}, che solitamente viene istanziato molteplici volte, su una distribuzione scalabile di Kayak, in modo da svolgere in parallelo quanto possibile.\newline
Prendendo ad esempio tasks di profilazione, che richiedono quindi l'archiviazione di meta-dati relativi ai datasets analizzati, si può subito intuire il ruolo cruciale di un \textbf{Metadata Catalog}, necessario per tenere organizzate queste informazioni. Esso riporta per ogni file dati di tipo inter-dataset ed intra-dataset, che permettono quindi di sapere qualcosa sia riguardo all'ambito di interesse del file, sia alle sue possibili correlazioni con gli altri elementi del data lake.\newline
Il popolamento ed il mantenimento di questo catalogo viene delegato al \textbf{Metadata Collector}. Esso offre varie funzioni per la generazione di informazioni intra-dataset e inter-dataset.
L'accesso ai dati viene delegato all'\textbf{Access Manager}, che offre interfacce per il file system, i databases e ai motori di processamento quali Spark. Esso implementa ed estende le tecniche basate su indici per accelerare l'accesso ai dati grezzi .\newline
Infine, Kayak è dotato di un \textbf{Recommender Engine} che guida l'utente suggerendo possibili query successive.\newline
Questi tools permettono l'utilizzo di una serie di primitive, ovvero di operazioni generali e suddivisibili in tasks. Tra le primitive più importanti si possono identificare, per dare un'idea più tangibile del funzionamento del sistema, quelle di "Inserimento di un dataset", "Rimozione di un dataset", "Ricerca di un dataset", "Profilazione completa", "Richiesta di suggerimento (recommendation)" e "Calcolo delle joinabilities".\newline
Entrando nello specifico di queste primitive, ad esempio con quella di inserimento, possiamo immaginare una sua potenziale suddivisione in tasks che portano al raggiungimento dell'obiettivo: in particolare, il dataset verrà sottoposto ad una "Profilazione Basilare" e successivamente all'"Inserimento nel file system", mentre una "Profilazione Completa" può procedere con profilazioni di tipo statistico, di calcolo delle joinabilities del dataset relativamente al lake ed eventualmente con altri tasks.
\section{Il Metadata Collector}
Come abbiamo detto, il \textit{Metadata Collector} si occupa di generare i metadati di profilazione dei datasets inseriti nel lago, seguendo quindi i nuovi files persino durante l'inserimeto, dai primi step di analisi fino a tipi di profilazione più complessi che verranno discussi nei capitoli successivi.
Il Metadata Collector è rappresentato da un insieme di funzioni e di connettori con Spark per l'elaborazione del dataset al suo inserimento in Kayak.
\subsection{Inserimento di un dataset}
Al momento dell'inserimento di un nuovo dataset, che può avvenire tramite API o tramite il client web di Kayak, ed in particolare seguendo il processo nel secondo caso, viene invocato il comando "InsertCommand" presente nell' "userInterface" del framework.\newline
Inizialmente vengono raccolti metadati di contorno con costo unitario, in particolare:
\begin{enumerate}
	\item \emph{Path del file}: riporta la directory nella quale risiede il file.
	\item \emph{Nome completo}: viene preso dal path.
	\item \emph{Data di creazione del file, ultimo accesso e ultima modifica}: che vengono raccolti dagli attributi base del file.
	\item \emph{Nome del dataset}: nome del file escludendone l'estensione.
	\item \emph{estensione}: riportata anche singolarmente e necessaria per interpretare il file, per questo lavoro è stata considerata solo l'estensione \textit{.csv}.
\end{enumerate}
In questa fase viene anche inizializzata, in caso si stia inserendo il primo dataset nel framework, la struttura dati globale che si occupa del calcolo delle joinabilities, di cui parleremo in un capitolo successivo.\newline
È stato anche inserito un controllo per evitare di inserire un dataset con lo stesso nome, quindi probabilmente già profilato, o che comunque potrebbe creare problemi successivamente, costringendo quindi l'utente a verificare di non star inserendo dati già presenti che, in caso negativo, potrà comunque inserire modificando il nome del nuovo file.\newline
A questo punto viene inizializzato il "DatasetProfile", una struttura che racchiude tutti i metadati generati relativamente al singolo dataset che fornisce metodi di lettura e scrittura su MongoDB, come vedremo successivamente, ed inoltre il dataset viene salvato nel contesto del datalake.\newline
\subsection{Raccolta dei metadati}
Vengono ora documentate le principali funzioni di Spark e di tipo "raw", ovvero non di Big Data, effettuate all'inserimento del file:
\begin{enumerate}
	\item \emph{conto delle righe} : un semplice conto della dimensione del file in termini di "datapoint", ovvero entry nel dataset;
	\item \emph{nomi e posizioni delle colonne} : importanti per successive elaborazioni e derivate dalla prima riga del file;
	\item \emph{frequenza degli elementi più frequenti} : una tra le strutture dati più importanti, di cui si avrà una descrizione più dettagliata nei capitoli successivi.
\end{enumerate}
\subsubsection{Conto delle righe}
Questa operazione viene effettuata chiamando il metodo "\textit{countRows}" di "\textit{RawOperations}", che prende in input il nome del file e lo apre in lettura.\newline
Una volta aperto il file, viene incrementato un accumulatore di tipo \textit{Long} mentre si scorre il file, e viene restituito il valore dell'accumulatore, che coincide con il numero di righe del file.\newline
\subsubsection{Nomi e posizioni delle colonne}
Le colonne vengono salvate sia come semplice lista che come una mappa per tenere conto della loro posizione nel file.\newline
Abbiamo quindi due strutture dati:
\begin{itemize}
	\item List\textless String\textgreater \textit{columns} : una semplice lista di \textit{Stringhe} riportanti le colonne;
	\item Map\textless String, Integer\textgreater \textit{columnsToPos} : questa mappa ha come valore per ogni \textit{entry} la posizione della colonna riportata nella chiave.
\end{itemize}
La prima lista viene generata chiamando il metodo "\textit{getColumnNames}" e passando come parametri:
\begin{itemize}
	\item \emph{\textit{String} fileName} : il nome del file;
	\item \emph{\textit{String} filePath} : il path del file;
	\item \emph{\textit{Boolean} hasHeader} : un valore booleano impostato a \textit{true} se il file contiene la riga degli header, \textit{false} altrimenti;
	\item \emph{\textit{Character} separator} : il carattere separatore del file (allo stato attuale l'unico effettivamente gestito è la virgola ",").
\end{itemize}
Questi parametri vengono presi tra gli altri in input dal client al momento dell'inserimento del dataset.\newline
Viene a questo punto inizializzata la lista che riporterà il risultato e viene aperto il file in lettura.\newline
Ovviamente verrà letta solo la prima riga, di interesse perché riportante i nomi delle colonne, in caso la variabile \textit{hasHeader} sia settata a \textit{true}.\newline
A questo punto viene considerato il carattere separatore, e se esso é una virgola, viene utilizzata la seguente espressione regolare per suddividere correttamente il file:\newline
\begin{center}
	\begin{Verbatim}[fontsize=\small, frame=single]
		  ,(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)
	\end{Verbatim}
\end{center}
In un file di tipo "csv", ovvero con campi separati da virgole, utilizzati in questo lavoro, vengono convenzionalmente utilizzate le doppie apici per racchiudere stringhe più lunghe di una parola e che magari possono contenere delle virgole in caso di testi discorsivi o descrittivi, come anche altri doppi apici. Inizialmente veniva usato un semplice \textit{split} tramite il singolo carattere ",", che chiaramente non permetteva di effettuare l'\textit{escape} delle virgole nelle situazioni appena descritte.\newline
Con questa \textit{regex} invece è possibile gestire files che riportino virgole all'interno dei campi, ammesso che il file in questione utilizzi la convenzione di racchiudere il campo in doppi apici.\newline
Alcuni datasets potrebbero riportare informazioni molto lunghe in determinate colonne, come ad esempio recensioni di prodotti. Questo tipo di dati vengono riportati, se il \textit{csv} é stato generato correttamente, tra virgolette \lq\rq, e la regex riesce ad accorgersene, quindi a non contarle nella suddivisione della riga considerata.\newline
Essa é resistente anche ad eventuali campi vuoti, nel qual caso troveremmo due virgole conseguenti .\newline
Le colonne vengono salvate in sequenza all'interno di una \textit{Lista} e quindi mantenendone l'ordine. In caso di assenza di un \textit{header} per il file, le colonne vengono contate e nominate con la dicitura ''\textit{ColumnN}'', dove \textit{N} rappresenta l'indice della colonna.\newline
Successivamente, questa lista con i nomi delle colonne, viene utilizzata per generare una mappa di tipo \textless String, Integer\textgreater, che riporta per ogni colonna il suo indice. La mappa viene restituita invocando il metodo "\textit{getNameToPosition}" del \textit{Metadata Collector}.\newline
\section{L'Ontology Extractor}
L'Ontology Extrator è un modulo aggiunto basandosi sull'idea di \textit{affinity} descritta nel paper di Kayak. Esso si occupa di raccogliere le \textit{ontologie}, che non assumono l'accezione filosofica del termine, bensì una rappresentazione formale, condivisa ed esplicita di una concettualizzazione di un dominio di interesse.\newline
Più nel dettaglio, si tratta di una teoria assiomatica del primo ordine esprimibile in una logica descrittiva. \cite{WIKI:ONTOLOGIA}\newline
Un'ontologia quindi in qualche modo rappresenta cosa sia la determinata entità al quale si riferisce: ad esempio delle ontologie per l'entità "Italia" potrebbero essere \textit{Place}, \textit{Country}, \textit{Location}, ecc..\newline
Identificare una lista di ontologie per un'entità, o per un insieme di entità, può quindi fornire importanti informazioni sul relativo dominio di interesse. Questo tipo di indizio può essere utile per mettere in relazione dei datasets, se i rispettivi attributi o una colonna in particolare di entrambi riportano sottoinsiemi delle stesse ontologie.\newline
I \textit{Knowledge Bases}, o "basi di conoscenza", sono degli speciali \textit{database} per la gestione della conoscenza per scopi aziendali, culturali o didattici. Essi costituiscono dunque un ambiente volto a facilitare la raccolta, l'organizzazione e la distribuzione della conoscenza.\newline
Alcuni tipi di KG si occupano di catalogare quante più informazioni possibili raccogliendole sotto l'entità a cui fanno riferimento, che sia essa una persona, un'animale, una cosa, una data, un evento, ecc..\newline
Tra le altre informazioni, essi riportano anche una lista di ontologie attribuibili alla data entità, utili per i nostri scopi. In particolare, l'\textit{Ontology Extractor}, sottomodulo del \textit{Metadata Collector}, offre un'interfaccia per la ricerca di ontologie su \textit{DBPedia}, uno tra i più popolari Knowledge Bases.\newline
DBPedia rappresenta uno sforzo collettivo per estrarre informazioni da Wikipedia e renderle disponibili sul Web. DBPedia descrive circa 4.5 milioni di entità, delle quali 4.2 sono classificate in ontologie coerenti, includendo 1.4 milioni di persone, 735,000 posti (di cui 478,000 luoghi popolati), 411,000 opere creative (tra le quali 123,000 album musicali, 87,000 films e 19,000 video games), 241,000 organizzazioni (tra cui 58,000 compagnie e 49,000 istituzioni educative), 251,000 specie e 6,000 malattie. \cite{DBPedia}\newline
\subsection{L'Ontology Facade}
In questa sottosezione viene descritta la \textit{Facade} del modulo, che si occupa semplicemente di prendere in input alcuni dei risultati del Metadata Collector, in particolari le eventuali chiavi identificate, la lista delle colonne e le frequenze degli elementi più frequenti.\newline
La facade è stata progettata per garantire in futuro un comodo interfacciamento ad altri KG per migliorare o arricchire la ricerca. Essa infatti allo stato attuale offre due soli metodi: \textit{getOntologyMapFromDBPedia} e \textit{filterAndSortOntologyMap}.\newline
In particolare il primo metodo, come detto, prepara i dati per l'interrogazione di DBPedia, istanziando inizialmente due strutture dati:
\begin{itemize}[noitemsep]
	\item \emph{ontologyMap} : una Map\textless String, Map\textless String, Integer \textgreater{}\textgreater 
	\item \emph{redirectsMap} : una Map\textless String, Map\textless String, String\textgreater{}\textgreater
\end{itemize}
La prima riporterà, per ogni colonna del dataset, una mappa rappresentante come chiave una determinata ontologia, e come valore numerico il numero di entità della relativa colonna a cui viene attribuito quel tipo di ontologia.\newline
La seconda struttura, seguendo l'organizzazione della prima, riporta eventuali \textit{redirects} al posto del numero di occorrenze dell'entità. Un redirect è il nome della pagina principale al quale punta la determinata entità, nel caso essa sia una sigla, come ad esempio ITA, o se è scritta in un linguaggio differente dall'inglese: l'entità "Italia" avrà come redirect "Italy". \newline
Questa struttura era stata pensata per migliorare la precisione per la joinability, comparando al posto delle entità i loro redirects, o anche per estrarre più ontologie da DBPedia interrogando anche la pagina di redirect, ma in seguito per la scarsa casistica di differenza dai valori presenti nei datasets, è rimasta in uso solo per il secondo scopo.\newline
Dopo l'inizializzazione di queste due strutture, viene effettuato un controllo sulle chiavi: se esse sono presenti, l'estrattore lavorerà solo su quelle colonne, mentre in caso DUCC non abbia rilevato uniques, verranno estratte entità per tutte le colonne del dataset.\newline
A questo punto vengono iterate le colonne selezionate, prendendo da ognuna 10 delle entità più frequenti; questo per evitare di arrivare fino a 100 richieste per colonna, considerando questo sottoinsieme sufficiente ad identificare le ontologie più rilevanti per la colonna.\newline
Viene poi istanziato un oggetto \textit{DBPediaQuery}, che andremo ad approfondire successivamente, che ritornerà poi alla facade un oggetto \textit{DBPediaResult}, del quale si parlerà più nel dettaglio in seguito.\newline
Infine, la struttura dati delle ontologie viene filtrata e ordinata dall'altro metodo della facade: in particolare, se una colonna possiede più di 25 diverse ontologie, eventuali occorrenze singole di una ontologia vengono rimosse dalla mappa, poiché probabilmente fuorvianti. Prendiamo ad esempio un insieme di società: tra le altre possiamo trovare la "\textit{Apple}", e la ricerca delle sue ontologie potrebbe riportare qualcosa come "\textit{Fruit}" o "\textit{Food}", decisamente fuorviante per identificare il contesto. Le mappe legate alle diverse colonne vengono quindi disposte in ordine decrescente sul numero di occorrenze.\newline
Un'analisi più approfondita sul funzionamento logico del modulo verrà presentata nei capitoli successivi.
\section{Il Metadata Catalog}
I dati e le strutture fino a qui descritte necessitano una strutturazione e una conseguente persistenza all'interno dello \textit{storage} di riferimento, ovvero MongoDB.\newline
Le operazioni fin qui descritte sono state delegate dalla \textit{MetadataFacade} del \textit{MetadataCatalog} al \textit{MetadataCollector}, a sua volta invocata dal \textit{MetadataManager} al momento dell'arrivo del nuovo dataset in input al sistema.\newline
Andiamo a vedere ora in che modo i singoli profili vengono conservati sul database: in particolare ogni profilo è rappresentato da una classe denominata \textit{DatasetProfile}, che possiede al suo interno variabili e oggetti rappresentanti diversi tipi di metadati.\newline
Nelle  successive sottosezioni viene descritto il DatasetProfile ed i tipi di metadati in esso contenuti.
\subsection{Il DatasetProfile}
Questa classe rappresenta l'insieme dei metadati che vanno a costituire un profilo per un determinato dataset. Tra gli altri metodi \textit{getter} e \textit{setter} per le sue variabili, presenta due funzioni di particolare importanza: una per il \textit{parsing} da oggetto Java a \textit{Document} per la persistenza su MongoDB, ed un costruttore che effettua il processo opposto conseguentemente all'interrogazione del database, per renderlo utilizzabile a livello di codice.\newline
La variabili da cui il DatasetProfile è composto sono le seguenti:
\begin{itemize}[noitemsep]
	\item UUID id;
	\item String fileName;
	\item double confidence;
	\item ContentMetadata contentMetadata;
	\item DependencyMetadata dependencyMetadata;
	\item DescriptiveMetadata descriptiveMetadata;
	\item LegalMetadata legalMetadata;
	\item ProvenanceMetadata provenanceMetadata;
	\item StatisticalMetadata statisticalMetadata;
	\item StructuralMetadata structuralMetadata;
	\item UsageMetadata usageMetadata;
	\item IndexesMetadata indexesMetadata;
	\item OntologyMetadata ontologyMetadata;
	\item ProfilingMetadata profilingMetadata.
\end{itemize}
Alcune di esse, quelle utilizzate nel mio lavoro, verranno presentate più nel dettaglio nella sottosezione successiva.\newline
La classe presenta un costruttore basilare che prende in input un UUID e il nome del dataset per instanziare l'oggetto all'inizio dell'iter di raccolta dei metadati, che verranno man mano aggiunti al profilo.\newline
Un'altro costruttore prende invece in input il Document estratto da MongoDB e imposta i valori delle variabili del DatasetProfile estraendole dal documento: per i primi 3 attributi, sempre presenti, basta una semplice \textit{get} sul documento, mentre per gli altri metadati si utilizza una struttura di questo tipo:
\begin{lstlisting}
this.metadataName = metadataDoc!= null ? 
new metadataName(metadataDoc) : null
\end{lstlisting}
In questo modo, se il \textit{metadataDoc} (di tipo Document) estratto da Mongo è presente, viene costruito il metadato utilizzando il costruttore della sua classe, in caso contrario viene passato come valore \textit{null}.\newline
Per quanto riguarda il processo inverso, con la funzione "\textit{toMongoParser}" viene restituito un Document pronto per l'inserimento sul database, utilizzando la funzione \textit{append} per aggiungere i vari metadati al documento. Anche in questo caso viene utilizzato un costrutto che garantisce l'inserimento della classe, se presente, o che gestisce la sua assenza impostando il campo ad un valore nullo:
\begin{lstlisting}
METADATA_NAME, this.metadataName != null ? this.metadataName.toMongoParser() : null
\end{lstlisting}
Come si può intuire, ogni metadato riscrive il suo metodo \textit{toMongoParser()} per copiare correttamente ogni campo.
\subsection{Le strutture dei metadati}
Come abbiamo visto, il DatasetProfile fa da contenitore per alcune strutture che riportano i metadati elaborati dal MetadataCollector.\newline
Andiamo a vedere i più importanti:
\begin{enumerate}
	\item \emph{DescriptiveMetadata} : in questa classe vengono salvati i dettagli relativi al file, ed in particolare:
	\begin{itemize}[noitemsep]
		\item nome
		\item formato
		\item data di creazione
		\item data dell'ultima modifica
		\item descrizione
	\end{itemize}
	In particolare, l'ultimo di questi attributi viene preso dall'input dell'utente, che può inserire questo valore per dare una breve descrizione del dataset che sta inserendo.
	\item \emph{StructuralMetadata} : questa è tra le sezioni più rilevanti del DatasetProfile, e riporta metadati estratti dal \textit{Collector} ed anche provenienti in input dal client Kayak.\newline
	Al suo interno vengono salvati il numero di colonne e righe del dataset, il booleano che rappresenta la presenza dell'header ed il carattere separatore.\newline
	Troviamo anche la lista delle colonne, le chiavi estratte, il tipo delle colonne e la mappa che ne riporta la posizione analizzata nel capitolo del Metadata Collector.\newline
	I dati relativi alle colonne, tra i quali l'ultimo citato, sono utili per l'accesso nelle varie strutture dati e sul dataset stesso.
	\item \emph{ContentMetadata} : questa struttura dati riporta i risultati più onerosi a livello computazionale del MetadataCollector: essa infatti contiene, oltre alla \textit{category label}, l'etichetta passata in input dal client, gli elementi più frequenti e la frequenza degli elementi più frequenti. Come si può facilmente intuire, il primo dato è ridondante rispetto al primo e apporta anche meno informazione, riportando solo gli elementi e non la frequenza con cui appaiono nel dataset.\newline
	Questa informazione veniva inizialmente ricalcolata scorrendo tutto il file, e l'iter di profilazione è stato ridotto in termini di tempo derivandola invece dall'altra, utilizzando un semplice \textit{keySet} su ogni mappa relativa alle colonne.
	\item \emph{ProvenanceMetadata} : in questa classe viene salvato il \textit{file path} di provenienza del dataset.
	\item \emph{OntologyMetadata} : in quest'ultima struttura dati vengono salvati i dati citati nella sezione precedente, ovvero la mappa delle ontologie e la mappa dei redirects.
\end{enumerate}