In questo capitolo viene mostrato un altro metodo di profilazione, che fa riferimento all'affinità tra due datasets, in particolare alla condivisione di un qualche dominio tra le rispettive entità.\newline
L'identificazione di questo tipo di relazione rientra nel concetto di affinità illustrato nel secondo capitolo, e consiste in un legame meno forte della joinability ma comunque di interesse e ad esso potenzialmente sinergico.\newline
Come visto nel primo capitolo, questo processo viene gestito dall'Ontology Extractor, il quale permette gestisce l'utilizzo del modulo che interroga direttamente il KG \textit{DBPedia}.\newline
Il capitolo è organizzato in due parti: nella prima viene presentata la tecnica di estrazione delle informazioni dalle pagine di DBPedia, mentre nella seconda si riportano alcune migliorie dedotte dall'analisi dei risultati offerti per incrementare la precisione del dominio identificato.
\section{La DBPediaQuery}
Il modulo per le query a DBPedia mantiene un riferimento al sito per effettuare la query, in una stringa:
\begin{Verbatim}
	http://dbpedia.org/resource/
\end{Verbatim}
Essa viene utilizzata concatenandola alle entità per la ricerca su DBPedia.\newline
Questo KG offre anche un servizio web che può essere utilizzato per cercare tra gli \textit{URI} di DBPedia con parole chiave correlate. Con correlate si intende che sia l'etichetta di una risorsa che un \textit{testo di ancoraggio} frequentemente utilizzato su Wikipedia per riferirsi ad una specifica risorsa vengono selezionate \cite{DBPediaLookup}.\newline
Il servizio in questione è chiamato \textit{DBPedia Lookup}, e trovando risorse in maniera cosi lasca, è risultato decisamente troppo generico per l'obiettivo da raggiungere.\newline
Si è quindi preferito accedere direttamente alla pagina dell'entità e da lì estrarre informazioni.\newline
Il modulo viene utilizzato invocando la funzione principale \textit{getInfoAndCountOntologies} a cui vengono passati la lista delle entità considerate, ovvero quelle relative ad una singola colonna, e l'\textit{URL} citato precedentemente.\newline
Vengono inizializzate le mappe che saranno i valori della mappa superiore indicizzata sulle colonne, una per le ontologie ed una per i redirects.\newline
\subsection{Verifica di validità}
Su ogni stringa presa come parametro, viene effettuato un controllo di validità eseguito dalla funzione "\textit{isValidResource}": essa si occupa di controllare inizialmente se l'input è vuoto o se rappresenta un URI, in qual caso non avrebbe senso effettuare una query al KG, come risulta dal risultato ottenuto in entrambi i casi concatenando la stringa all'URL di base:
\begin{itemize}[noitemsep]
\item http://dbpedia.org/resource/
\item http://dbpedia.org/resource/http://google.it
\end{itemize}
A questo punto, vengono analizzati i singoli caratteri della stringa per verificare la loro validità per la costruzione di un URL corretto, confrontandoli con la sequenza dei caratteri ammissibili:
\begin{center}
	\begin{Verbatim}[fontsize=\small, frame=single]
	ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz
		0123456789-._~:/?#[]@!$&'()*+,;=`. \"
	\end{Verbatim}
\end{center}
Se tutte i caratteri dell'input si trovano tra gli ammissibili, la funzione restituisce \textit{true} e si può proseguire.
\subsection{Replacement dei caratteri}
Durante i test effettuati, è risultato evidente come alcuni caratteri creassero problemi sia per lo storage sul database sia per effettuare le query a DBPedia, quindi alcuni vengono rimossi prima di costruire l'URL:
\begin{itemize}[noitemsep]
\item eventuali caratteri spazio ' ' vengono rimpiazzati con degli \textit{underscore} '\_';
\item i caratteri \textit{backslash} '\textbackslash' vengono rimossi;
\item estensioni di tipo '.json' vengono eliminate;
\item virgolette '"' presenti nella stringa vengono rimosse.
\end{itemize}
\subsection{Acquisizione della pagina}
A questo punto la stringa è pronta per essere concatenata all'URL ed effettuare la query. Il controllo viene passato alla funzione "\textit{getDBPediaPage}", che prende in input proprio l'unione del link a DBPedia con l'entità da ricercare.\newline
Viene effettuato un nuovo controllo rimuovendo eventuali virgolette '"' e rimpiazzando virgole ',' con punti '.'.\newline
Sono state utilizzate le librerie di Java per le connessioni \textit{HTTP}, aprendo la connessione di tipo "\textit{HttpURLConnection}".\newline
Dopo aver impostato diversi parametri come l'\textit{user agent} e la possibilità di avere il risultato in formato "json", viene effettuata la richiesta al KG, che viene raccolta in uno "\textit{StringBuffer}" concatenando le varie righe di risposta ottenute in un \textit{BufferedReader}.\newline
A questo punto il risultato viene trasformato in un \textit{JSONObject} utilizzando il suo costruttore con parametro la stringa ottenuta dallo StringBuffer invocando il metodo \textit{toString()} e viene ritornato alla funzione principale.
\subsection{JSON to Map}
Per elaborare in maniera migliore il risultato di DBPedia, si é resa necessaria una traduzione ad un formato più efficiente: in particolare il JSONObject viene trasformato in una Map\textless String, Object\textgreater, nella quale ogni Object può assumere qualsiasi tipo di valore, anche una nuova mappa dello stesso tipo.\newline
La trasformazione viene effettuata basandosi sul riutilizzo di codice trovato sul web\footnote{\url{https://stackoverflow.com/questions/21720759/convert-a-json-string-to-a-hashmap}}.\newline
La mappa viene ritornata alla funzione principale che può continuare l'esecuzione.
\subsection{Estrazione dei redirects}
Analizzando la struttura dei JSON restituiti da DBPedia, è stata rilevata la presenza del campo rappresentante i redirects. Esso si trova accedendo alla mappa indicizzata sulla stringa seguente al "primo livello" della struttura:
\begin{itemize}
\item http://dbpedia.org/resource/ + "resource"
\end{itemize}
dove ovviamente \textit{"resource"} coincide con l'entità considerata. Come si può notare, il link coincide con quello dell'URL utilizzato per interrogare il KG.\newline
Dalla mappa ottenuta viene estratto il \textit{keySet}, ovvero la lista di tutte le chiavi, e tra di esse ne viene cercata una contenente la stringa "\textit{wikiPageRedirects}", che contine appunto gli eventuali redirects.\newline
Se essa viene trovata, la stringa di cui fa parte viene utilizzata per estrarre la nuova mappa, che viene \textit{parsata} come List\textless Map\textless String, Object\textgreater{}\textgreater, ma contenente in realtà un solo elemento con due chiavi: \textit{type} e \textit{value}.\newline
Dei due valori, quello di interesse è il secondo, value, che riporta l'URI di redirect, della stessa struttura mostrato in precedenza, da cui viene rimossa la parte iniziale (http://dbpedia.org/resource/) e che viene restituito come output. In caso il campo non fosse presente, essendo l'entità considerabile principale, viene restituita l'entità stessa.\newline
\subsection{Estrazione delle ontologie}
L'estrazione delle ontologie dalla pagina viene effettuata accedendo alla stessa chiave utilizzata per i redirects, ma al secondo livello viene cercata una stringa diversa tra le chiavi, ovvero "\textit{rdf-syntax-ns\#type}", che è risultata indicizzare la lista delle ontologie relative all'entità.\newline
Identificata la chiave, essa viene utilizzata per estrarre la List\textless Map\textless String, Object\textgreater{}\textgreater, iterando la quale e tagliando la porzione iniziale dei campi \textit{value} otterremo la lista delle ontologie da ritornare alla funzione principale.
\subsection{Estrazione delle disambiguazioni}
Una stringa potrebbe avere significato per un diverso numero di entità, come nel caso della parola "Apple" visto precedentemente. Per questo motivo, su ogni elemento viene effettuato anche un controllo per capire se la pagina è di disambiguazione e, in tal caso, la ricerca delle ontologie viene estesa anche alle pagine a cui quella in questione fa riferimento.\newline
Le entità di disambiguazione vengono estratte utilizzando la stessa procedura utilizzata per i redirects e le ontologie, con la differenza che nella seconda mappa va ricercata una chiave differente:
\begin{itemize}
\item http://dbpedia.org/ontology/wikiPageDisambiguates
\end{itemize}
Se essa è presente, verrà parsata in una List\textless Map\textless String,Object\textgreater{}\textgreater, ed i valori verranno estratti come visto nella sezione precedente dal campo value, per poi essere restituiti in output. Da notare che in questo caso verrà restituita una lista di dimensione variabile in base alle disambiguazioni presenti. La ricerca di queste disambiguazioni ha un valore chiave nella comprensione del contesto, e non porta ad errori, come si potrebbe pensare inizialmente.\newline
Infatti, prendendo ancora ad esempio la stringa "Apple", raccogliendo le ontologie per le disambiguazioni "Apple(fruit)" ed "Apple(company)", avremo sicuramente risultati legati sia al frutto che alla società informatica. Tuttavia, se nella lista di entità sono presenti altri tipi di frutti, l'ontologia legata a questi cibi aumenterà in numero di occorrenze, mentre probabilmente quella legata alla società avrà una o poche più occorrenze, facendo arrivare a concludere che si sta parlando di alimenti e non di aziende.\newline
\subsection{Conteggio dei valori}
Una volta estratte ontologie, redirects e disambiguations, nelle rispettive strutture dati, ovvero liste di Stringhe per la prima e la terza ed una semplice stringa per la seconda, se presente, si passa al loro conteggio e successiva addizione per riportare i risultati.\newline
Per quanto riguarda le ontologie, esse vengono scorse e via via aggiunte ad una mappa contenente \textit{Integer}, valore rappresentante il numero delle occorrenze. Un'eventuale entità già presente farà aumentare l'intero di uno.\newline
La mappa viene poi ordinata e restituita alla funzione principale.\newline
Le disambiguations invece, non rappresentando ontologie ma entità a loro volta, vengono utilizzate per effettuare nuove query a DBPedia ed estrarre le ontologie con il metodo discusso precedentemente.\newline
Avremo quindi una Map\textless String, List\textless String\textgreater{}\textgreater riportante una lista di ontologie trovata per ogni disambiguazione. Questa mappa viene scorsa e le rispettive liste iterate per popolare una mappa della stessa struttura di quella appena illustrata per il conteggio delle ontologie a cui verrà unita, come intuibile, sommando eventuali occorrenze identiche, che danno peso al ragionamento sull'utilità delle disambiguations.\newline
A questo punto le ontologie risultanti dai due conteggi vengono aggiunte alla \textit{ontologiesMap} istanziata all'inizio della funzione, e con la quale condividono la struttura. Questa mappa viene poi nuovamente ordinata ed utilizzata, insieme alla mappa dei redirects, per costruire un oggetto di tipo \textit{DBPediaResult}, un semplice \textit{wrapper} per poter restituire le due strutture dati.
\section{Controlli finali post-processamento}
Prima di venire utilizzata per costruire l'oggetto appena citato, la mappa delle ontologie viene scorsa per eliminare determinati valori ricorrenti in tutte le pagine o comunque poco rilevanti. Dall'analisi dei risultati, infatti, è emersa l'inutilità di determinati valori:
\begin{itemize}
	\item ontologie contenenti numeri, poiché solitamente indicano riferimenti a determinate entità su altri KG;
	\item ontologie provenienti dal KG \textit{Yago}, identificate proprio dal nome presente nella stringa;
	\item l'ontologia "\textit{owl\#Thing}", trovata praticamente in ogni pagina, essendo ogni entità identificabile come \textit{Thing}, cioè una \textit{cosa}.
\end{itemize}
L'oggetto DBPediaResult viene ritornato alla OntologyFacade, che stava iterando le entità più frequenti del dataset, la quale aggiunge man mano le due strutture degli oggetti che gli vengono ritornati alle mappe discusse nella sezione precedente.\newline
Al termine del ciclo, la mappa delle ontologie viene iterata, e per ogni chiave, coincidente con la colonna del dataset, viene filtrata la mappa relativa da eventuali entità ricorrenti una sola volta se supera una determinata soglia di dimensione, in questo caso impostata a 25 \textit{entry}. Le mappe della ontologiesMap vengono ordinate nuovamente, ed infine inserite in una classe wrapper chiamata \textit{OntologyResult}.\newline
Essa è composta dalle due variabili: 
\begin{itemize}[noitemsep]
\item Map\textless String, Map\textless String, Integer\textgreater{}\textgreater{}  ontologiesMap;
\item Map\textless String, Map\textless String, String\textgreater{}\textgreater{} redirectsMap.
\end{itemize}
che riportano, come facilmente deducibile dalla trattazione, per ogni colonna del dataset, la lista delle ontologie identificate con le loro occorrenze, e la lista dei redirects per ognuna delle entità selezionate dalle più frequenti. Proprio questa selezione effettuata per evitare fino a 100 richieste HTTP per colonna ha reso poco utilizzabile questa struttura dati, poiché il confronto di essi con il sottoinsieme di redirects di un altro dataset sarebbe limitato a sole 10 tra le entità più frequenti. Ciò non escluderebbe tuttavia una successiva ricerca di tutti i redirects su coppie di colonne con alta \textit{joinability}, per il miglioramento della cardinalità di un eventuale join tra i due datasets.