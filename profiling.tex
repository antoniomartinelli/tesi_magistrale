Le tecniche fin qui descritte possono essere utilizzate in diverse combinazioni grazie alla loro modularità ed indipendenza, offrendo differenti approcci che possono privilegiare la precisione a discapito dell'attesa della risposta o viceversa.\newline
Si può quindi definire un flusso che il dataset segue al momento dell'inserimento nel data lake, che andrà pian piano a popolare il relativo DatasetProfile.\newline
I metadati raccolti in questo iter, tuttavia, considerati nella loro identità singola e legata al loro dataset, sono inutili, se non nel contesto del file stesso, e non ci danno alcun indizio su come metterlo in correlazione con gli altri datasets presenti nel data lake.\newline
Si rende quindi necessario l'inserimento di un collante tra i tanti files presenti nel framework: è qui che viene introdotto il \textit{Global Dataset Metadata}, il quale permette di identificare i datasets più rilevanti ai fini di un eventuale join con un dataset selezionato. Esso consente di effettuare valutazioni sulla joinability e sull'affinità, quindi di produrre metadati di tipo intra-datasets.\newline
Il resto del capitolo è così organizzato: nella prossima sezione viene descritto l'iter di profilazione con le possibilità offerte dai diversi metodi discussi, nella seguente viene illustrata la struttura del Global Dataset Metadata, in quella successiva viene mostrato come effettivamente aiuta nel correlare i datasets all'interno di Kayak e nell'ultima viene descritto brevemente come vengono salvati i risultati.
\section{L'iter di profilazione}
Come abbiamo visto nei capitoli precedenti, ci sono diverse tecniche applicabili per la profilazione di un file: alcune possono essere sinergiche, altre opzionali o mutualmente esclusive.\newline
Esiste anche lo scenario per cui una tecnica può semplicemente consistere nell'approssimazione dei risultati di un'altra, con derivanti benefici in termini di tempo.\newline
I vari approcci fin qui descritti possono quindi essere combinati in diversi modi, per dare priorità ai tempi di risposta o alla precisione dei risultati.\newline
 \begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figure/profilingIter}
	\caption{Iter di profilazione su Kayak}
	\label{fig:profilingIter}
\end{figure}
\newline
Si può quindi definire un iter di profilazione schematizzato in figura \ref{fig:kayakArc} che parte dall'inserimento del dataset nel framework effettuando una profilazione basilare, di cui si è parlato nel primo capitolo, che raccoglie metadati di contorno non utili a generare quelli inter-datasets.\newline
A questo punto si passa alle tecniche descritte nei capitoli precedenti, con l'opzione di utilizzare l'algoritmo DUCC per provare ad identificare le chiavi, che poi verrebbero utilizzate per analizzare un subset delle colonne del dataset invece che tutte per estrapolare le entità più frequenti.\newline
Si ha poi la possibilità di calcolare questi elementi con il metodo standard oppure applicando il sampling preliminare al file.\newline
Infine, si può scegliere se passare i risultati fin qui ottenuti all'estrattore delle ontologie, per un'analisi sul contesto degli elementi trovati nelle diverse colonne.\newline
Tutti i dati estrapolati vengono poi salvati sul database nella struttura DatasetProfile discussa precedentemente.\newline
La modularità delle varie tecniche permette dunque diversi approcci con precisioni e tempi di esecuzione variabili che verranno analizzate nell'ultimo capitolo.\newline
Per riassumere, nella profilazione iniziale viene istanziato il DatasetProfile relativo al file in input che verrà via via arricchito con le profilazioni successive, che riguardano inizialmente tecniche di estrazione della colonna chiave per velocizzare la seguente raccolta degli elementi più frequenti, per cogliere in seguito relazioni di joinability tra i files.\newline
Questi dati vengono quindi utilizzati per l'estrazione delle ontologie, che necessita del calcolo dei "Top Frequent" proprio per validarli ulteriormente: infatti due datasets con alta joinability potrebbero risultare utili in seguito ad una aggregazione, e l'identificazione di uno stesso contesto può dare una garanzia ulteriore al riguardo.\newline
Tuttavia, come deducibile, i metadati fin qui estratti sono orientati alla conoscenza del singolo dataset e del contesto a cui appartiene, senza la possibilità di avere informazioni su relazioni di affinità o joinability che potrebbero legarli agli altri elementi del data lake.
\section{La struttura del Global Dataset Metadata}
Come abbiamo detto, il Global Dataset Metadata è una struttura che mette in relazione i datasets disponibili. I risultati della sua interrogazione vengono comunque salvati nel DatasetProfile del dataset in input. L'idea è di far capire, per ogni colonna, quali tra le colonne degli altri files sono più predisposte ad un join. Possiamo intuire che esse conterranno entità simili o almeno affini, appartenenti allo stesso ambito.\newline
Il Global Dataset Metadata sfrutta i dati salvati nel Content Metadata e nell'Ontology Metadata, se presenti, generati dall'iter di profilazione scelto, ed è organizzato in due strutture separate:
\begin{itemize}
\item Map\textless String, Map\textless String, Integer\textgreater{}\textgreater{} entitiesMap;
\item Map\textless String, Map\textless String, Integer\textgreater{}\textgreater{} ontologiesMap;
\end{itemize}
Come risulta ad una prima occhiata, esse sono formate dagli stessi tipi, ma hanno un'organizzazione differente rispetto a quelle relative ai singoli datasets dalle quali vengono costruite: infatti la mappa più esterna é indicizzata su chiavi che riportano, rispettivamente, le entità e le ontologie identificate nei vari datasets del data lake.\newline
Ognuna di queste chiavi ha come valore, a sua volta, una mappa di tipo \textless String, Integer\textgreater{}, le cui chiavi sono rappresentate da una composizione del nome del dataset e della colonna.\newline
Questa chiave è stata costruita inserendo una serie di caratteri tra i due valori sopra citati, poiché Mongo non accetta oggetti ad hoc per le chiavi.\newline
In particolare, è stata utilizzata questa serie di caratteri: 
\begin{lstlisting}[frame=none, xleftmargin=.4\textwidth]
|%#-#%|
\end{lstlisting}
In questo modo quindi le chiavi hanno una forma del tipo: 
\begin{itemize}
	\item \textit{DatasetName{|\%\#-\#\%|ColumnName}}
\end{itemize}
Questo perché, essendo l'input estremamente variabile, un semplice "-" a dividere i due campi non sarebbe stato sufficiente: se infatti il nome del file dovesse contenere a sua volta un "-", lo split della stringa chiave per effettuare considerazioni sul dataset e la colonna avrebbe portato certamente ad errori.\newline
Una serie di caratteri così composta può dare con buona probabilità la sicurezza di riuscire a dividere correttamente la chiave per effettuare confronti tra i contenuti dei datasets.\newline
All'interno della prima mappa si trovano quindi le entità identificate finora nel data lake, che mantengono come valore una mappa di chiavi composte come appena illustrato, che a loro volta riportano il numero di elementi più frequenti calcolati per quel dataset, valore che risulterà utile nel calcolo della joinability.\newline
Stessa organizzazione segue la mappa delle ontologie, che, però, riporta il numero di occorrenze della data ontologia trovata su DBPedia tramite l'estrattore di ontologie.\newline
Questa struttura dati viene mantenuta su una differente \textit{collezione} del database, in modo da non interferire con i singoli profili dei datasets.\newline
Ad ogni inserimento di un nuovo file in Kayak, il \textit{MetadataManager} controlla se il Global Dataset Metadata è presente nel database e, in caso contrario, si occupa di crearlo semplicemente inizializzando due mappe vuote identiche a quelle mostrate nella sezione precedente, inserendole in un documento da aggiungere alla collezione specifica di Mongo.\newline
L'interfaccia grafica di Kayak permette anche la rimozione di un singolo dataset: in questo caso, anche i riferimenti nel GDM vanno adattati, poiché il riferimento ad alcuni dataset per una data entità potrebbero indurre l'utente a voler effettuare un join con un file non più presente nel sistema.\newline
Il dataset da rimuovere viene richiamato dal database e, se esiste, vengono cercati i metadati relativi agli elementi più frequenti e alle ontologie.\newline
Se essi sono presenti, vengono utilizzati per accedere alle entry del GDM per rimuovere tutti gli eventuali riferimenti a quel file.\newline
Infine, il connettore alla collection dei DatasetProfile, rimuove il profilo da MongoDB.\newline
Se invece viene resettato il sistema eliminando tutti i metadati, tramite l'apposito bottone di "\textit{Reset}", si azzera anche la \textit{collection} del database relativa al GDM.
\section{L'utilizzo del GDM}
Viene ora evidenziata l'utilità del Global Dataset Metadata. Come abbiamo visto, esso viene popolato man mano che vengono inseriti nuovi datasets ed aggiornato se rimossi.\newline
Quando per un file è stato costruito il DatasetProfile, esso verrà mostrato nel \textit{Metadata Catalog} del client web di Kayak.\newline
Da qui, si può utilizzare il tasto "\textit{profileDataset}" accanto al nome per metterlo in relazione con gli altri del data lake. Il server Kayak quindi estrae il profilo del dataset in questione ed il Global Dataset Metadata.\newline
Dal loro confronto vengono estratte 2 diverse strutture dati:
\begin{itemize}
\item Map\textless String, Map\textless String, Double\textgreater{}\textgreater{} joinabilities;
\item Map\textless String, Map\textless String, Map\textless String, Long\textgreater{}\textgreater{}\textgreater{} commonOntologies.
\end{itemize}
La prima si ottiene confrontando la mappa della frequenza degli elementi più frequenti con la mappa delle entità del GDM.\newline
Viene inizializzata la mappa delle joinabilities e si itera sulle colonne del dataset in input, che contengono gli elementi più frequenti. Per ognuna viene creata una mappa di riferimento legata al nome nella mappa "joinabilities".\newline
All'interno si scorrono le entità appartenenti ad ognuna delle colonne, e, se la singola occorrenza viene trovata tra quelle salvate nel GDM, la mappa dei datasets in cui è presente viene estratta e iterata: ad ogni ciclo si controlla se il nome del dataset presente nella chiave non sia uguale a quello del dataset in input, poiché rappresenterebbe proprio l'entità in considerazione in questo momento, e non avrebbe senso computare la joinability di una colonna con se stessa; in caso contrario viene inizializzato un valore per la joinability calcolato come nella formula vista nel primo capitolo, dove l'1 sta ad indicare la proiezione di una singola entità, ovvero quella in questione, dopo un eventuale join.\newline
Al denominatore troviamo il numero delle entità più frequenti nella colonna del dataset in input sommato a quello della colonna del dataset trovato nel GDM.\newline
Per come è implementato il Metadata Collector i valori da ricercare nel GDM saranno al più 100.\newline
A questo punto, dalla mappa delle joinabilities viene estratta la mappa relativa alla colonna del dataset in input che si sta attualmente esplorando e, se la chiave "dataset|\%\#-\#\%|colonna" viene trovata, la joinability viene incrementata del valore appena discusso, altrimenti la nuova chiave viene inserita con l'attuale valore.\newline
Alla fine avremo quindi una mappa di joinabilities che, per ogni colonna del dataset in input, contiene una mappa indicizzata su "dataset|\%\#-\#\%|colonna" e riportante l'indice di joinability approssimato su al più 100 degli elementi più frequenti dei due.\newline
L'approssimazione è accettabile per alti valori della joinability, poiché a prescindere da eventuali altre \textit{entry} non coincidenti, avremo comunque un buon sottoinsieme di entità che coincidono tra i due file. Per quanto riguarda invece valori più bassi, il risultato potrebbe derivare sia da una effettiva non coincidenza dei valori, sia dal fatto che al momento dell'estrazione degli elementi più frequenti siano stati estratti valori diversi ma dello stesso dominio.\newline
In parole povere, prendiamo ad esempio due datasets, qui "\textit{ds1}" e "\textit{ds2}": ognuno di essi potrebbe in una determinata colonna riportare i nomi degli Stati del mondo, ma in ordine diverso. Il Metadata Collector potrebbe a questo punto estrarre come elementi più frequenti per ds1 un insieme di nomi di stati, ad esempio, che iniziano per lettere dalla A alla M, mentre per ds2 potrebbe aver considerato come elementi più frequenti quelli dalla N in poi, risultando in una joinability nulla errata.\newline
Il rischio di questa eventualità è tuttavia da mettere in conto ed accettare poiché la ricerca su tutte le entità del dataset, ammesso che siano di ordini di grandezza superiori, richiederebbe una gran quantità di tempo di elaborazione in più.\newline
Prima di essere restituita al flusso di esecuzione, la mappa viene esplorata e per ogni colonna i risultati vengono ordinati in maniera decrescente sul valore della joinability, avendo quindi per primi i risultati più interessanti.\newline
Per quanto riguarda il confronto delle ontologie, il procedimento è a grandi linee lo stesso visto per le entità.\newline
In questo caso viene effettuato inizialmente un controllo per la presenza della mappa delle ontologie nel profilo del dataset in input, poiché potrebbero anche non essere state identificate ontologie, sia per mancanza effettiva su DBPedia sia per l'ambiguità dei valori.\newline
Va considerato che in questo caso non avremo il risultato di una formula nel valore più annidiato dell'output, ma la somma delle occorrenze della determinata ontologia nelle colonne dei due datasets.\newline
Anche qui, per ogni colonna e per ogni "dataset|\%\#-\#\%|colonna", le mappe delle ontologie vengono ordinate decrescentemente sul valore.\newline
A questo punto si possono effettuare ulteriori considerazioni utilizzando i due risultati e non più accedendo al GDM: in particolare gli output dei due confronti possono essere a loro volta combinati per migliorare i risultati.\newline
Iterando le colonne delle entità in comune, e inizializzando una nuova mappa che conterrà il risultato con gli stessi nomi delle colonne come chiavi, per ognuna vengono scorsi i dataset per i quali sono stati identificati dei \textit{matchings}.\newline
All'interno della mappa delle ontologie viene ricercata la stessa colonna e la stessa coppia di chiave dataset-colonna, per verificare la presenza di ontologie in comune con l'altro dataset. Questo controllo è associato a quello sul valore della joinability tra le due colonne, e viene passato solo in caso che il valore superi la  "\textit{threshold}", ovvero la soglia passata in input alla funzione.\newline
In caso positivo, viene salvato il numero delle ontologie in comune e viene inserito nella mappa dei \textit{matchings} insieme alla chiave che identifica dataset e colonna esterni. A sua volta questa mappa viene inserita nella mappa principale relativa alle colonne del dataset input.\newline
A questo punto, la mappa costruita è del tipo Map\textless String, Map\textless String, Long\textgreater{}\textgreater{}, e riporta le colonne del dataset in input, che contengono mappe con chiavi "\textit{dataset|\%\#-\#\%|colonna}", che a loro volta riportano come valore il numero di ontologie in comune tra i due.\newline
Questa mappa viene filtrata prima di essere ritornata: vengono eliminate eventuali colonne che non hanno matchings, per ridurre la dimensione dell'output, ed ovviamente le chiavi rappresentanti i dataset esterni vengono ordinati in maniera decrescente sul numero di ontologie in comune.\newline
L'ultima struttura dati generabile tramite l'output dell'utilizzo del GDM, ed in particolare utilizzando quello della joinability basata sulle entità, estraendo dalla struttura dati le colonne chiave identificate dall'algoritmo DUCC tramite una semplice iterazione di quest'ultime, presenti nella sezione Structural Metadata del Dataset Profile.
\section{Profiling Metadata}
I risultati fin qui estratti vengono infine salvati nella sezione del Dataset Profile denominata Profiling Metadata, citata precendentemente.\newline
Essa è composta da 4 campi differenti, di seguito elencati:
\begin{itemize}
\item Map\textless String, Map\textless String, Map\textless String, Long\textgreater{}\textgreater{}\textgreater{} commonOntologies;
\item Map\textless String, Map\textless  String, Double\textgreater{}\textgreater{} joinabilities;
\item Map\textless String, Map\textless String, Double\textgreater{}\textgreater{} bestKeyMatches;
\item Map\textless String, Map\textless String, Long\textgreater{}\textgreater{} entitiesAndOntologiesMatchings.
\end{itemize}
La prima riporta le colonne degli altri dataset con ontologie in comune e il numero totale di occorrenze per ogni ontologia, la seconda riporta invece gli indici di joinability, la terza un subset della precedente prendendo solo le colonne del dataset in input identificate come chiave dall'algoritmo DUCC, mentre l'ultima contiene le colonne che hanno un indice di joinability superiore alla soglia impostata ed il numero di ontologie in comune che hanno con il dataset analizzato.